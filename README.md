# Robot Device

Library based on device components for robotics.

## Getting started

You can add to your project using CMake or install as a system library (see below).

The documentation is available in: [https://uc3m-sofia.gitlab.io/robot-device/](https://uc3m-sofia.gitlab.io/robot-device/)


## Name
Robot device library.

## Description
The library allows to define and control robotic devices like joints, sensors and others. 
It is object oriented and intends to use one class for each separate device. The library is centered in the concept of device, getting together in one class the actuators and sensors needed to perform an action.

These devices should run hardware independent in a similar way. The three initial hardware platforms considered are:

 - CanBus CiA &rarr; CiADevice
 - General purpose I/O devices over linux &rarr; GPIODevice
 - USB connected devices &rarr; USBDevice

Other platforms can be considered in the future.

## Badges
## Visuals

## Installation
This library is intended for the use with CMake build system.

### In project
For in project CMake build, find all the include and link directories in variables:

```cmake
SUBDIR_INCLUDE_DIRECTORIES
SUBDIR_LINK_NAMES
```

Then, assuming the library is placed at "${PROJECT_SOURCE_DIR}/lib/robot-device/" (for example after clone), it is enough to add the following lines to CMakeLists.txt to add includes:

```cmake
add_subdirectory(${PROJECT_SOURCE_DIR}/lib/robot-device/)
INCLUDE_DIRECTORIES(${SUBDIR_INCLUDE_DIRECTORIES})
```

Also after "add_executable( ${name} ${sourcefile} )" line, add the following to link the library:

```cmake
target_link_libraries( ${PROJECT_NAME} ${SUBDIR_LINK_NAMES} )
```
### As a system library
Use the usual CMake and make install commands in order to compile and install library on the system

```
git clone https://gitlab.com/uc3m-sofia/robot-device.git
mkdir .robot-device-build
cd .robot-device-build
cmake ../robot-device
make
make install
```

Then use FindRobotDevice.cmake file from ( TODO ) in order to use CMake find_package(RobotDevice) directive.

You can also uninstall the library with:

```
make uninstall
```
inside .robot-device-build directory.

## Usage

See [examples](https://gitlab.com/uc3m-sofia/robot-device-examples/) for an example of usage.

## Support


## Roadmap

- First milestone: define robot joint class

## Contributing


## Authors and acknowledgment

## License

## Project status

