#include "NiclaVision.h"

NiclaVision::NiclaVision(CommBase &nport, double ndts) : Sensors (nport)
{
    state.resize(2);
    //    for(int i=0; i<4; i++)
    //    {
    //        cout << port->receive() << endl;
    //    }
    dts=ndts;
    port->send("t");
    port->send(to_string(dts));
//    cout << port->receive() << endl;

    state.resize(3);
    rpy.resize(3);
    ioa.resize(3);
    abg.resize(3);


    atp.resize(3);
    atr.resize(3);
}

vector<double> NiclaVision::get(string what)
{
    switch (what[0])
    {
    case 'a' :
        port->send(what);

        state.resize(2);
        state[0] = stod(port->receive());
        state[1] = stod(port->receive());

        break;

    case 'd' :
        port->send(what);
        cout << "received: " << port->receive() << endl;

        break;

    default:
        state.resize(1);
        port->send(what);
        state[0] = stod(port->receive());
        break;

    }


    //    string rec;
    //    rec = port->receive();
    //    cout << "rec: " << rec << endl;

    //    stringstream values(rec);

    //    values >> state[0];

    return state;
}

vector< vector<double> > NiclaVision::GetAll()
{
    port->send("GetAll");

    for (int i=0; i<4;i++)
    {
        ReadFormattedLine(port->receive());
    }

    vector< vector<double> > ret={rpy,atp,atr,abg};

    return ret;


}

long NiclaVision::PrintAll()
{
    vector< vector<double> > v0=GetAll();
    cout << "imu: " << v0[0][0]<< ", " << v0[0][1]<< ", " << v0[0][2] << endl;
    cout << "atp: " << v0[1][0]<< ", " << v0[1][1]<< ", " << v0[1][2] << endl;
    cout << "atr: " << v0[2][0]<< ", " << v0[2][1]<< ", " << v0[2][2] << endl;
    cout << "abg: " << v0[3][0]<< ", " << v0[3][1]<< ", " << v0[3][2] << endl;


    return 0;

}

string NiclaVision::GetLine()
{
    return port->receive();

}

long NiclaVision::flush(long i)
{
    string rec;
    //Get port buffer size before receive

    // or use a non blocking flushing function
    //    rec = port->receive();
    cout << "rec: " << rec << endl;

    return 0;
}

vector<double> NiclaVision::to_vector(string formatted_3vector)
{
    stringstream ss(formatted_3vector);
    string test;
    vector<double> ret(3);

    ss >> test;
    ss >> test;
    ret[0] = stod(test);
    ss >> test;
    ret[1] = stod(test);
    ss >> test;
    ret[2] = stod(test);

//    cout << "vec: " << ret[0] << ret[1] << ret[2] << endl;

    return ret;
}

long NiclaVision::ReadFormattedLine(string fline)
{

    switch(fline[0])
    {

    case 'r':
        rpy=to_vector(fline);
        break;
    case 'i':
        ioa=to_vector(fline);
        break;
    case 'a':
        if (fline[2]=='p')
        {
            atp=to_vector(fline);
        }
        if (fline[2]=='r')
        {
            atr=to_vector(fline);
        }
        if (fline[1]=='b')
        {
            abg=to_vector(fline);
        }
        break;
    default:
        break;

    }

    return 0;


}
