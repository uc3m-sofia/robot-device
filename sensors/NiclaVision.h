#ifndef NICLAVISION_H
#define NICLAVISION_H

#include "Device/Sensors.h"

class NiclaVision : public Sensors
{
public:
    NiclaVision(CommBase &nport, double ndts=0.01);
    vector<double> get(string what);
    vector<vector<double> > GetAll();

    long PrintAll();
    string GetLine();
    long flush(long i=0);
    long PrintRpy();

private:
    vector<double> state;
    vector<double> rpy; //IMU roll pitch yaw
    vector<double> ioa; //IMU incli ori azimuth
    vector<double> abg; //IMU alfa beta gama (intrinsic XYZ)

    vector<double> atp; //april tag position
    vector<double> atr; //april tag rotation

    string portline;

    double dts;

    vector<double> to_vector(string formatted_3vector);
    long ReadFormattedLine(string fline);

};

#endif // NICLAVISION_H
