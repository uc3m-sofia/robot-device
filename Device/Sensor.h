#ifndef SENSOR_H
#define SENSOR_H
#include <string>
class Sensor{
  public:
    Sensor();
    virtual double get(std::string)=0;
};

#endif //SENSOR_H
