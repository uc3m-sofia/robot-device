#ifndef SENSORS_H
#define SENSORS_H

#include "../Comms/Comms.h"

class Sensors
{
public:
    Sensors(CommBase &nport);
    virtual vector<double> get(string)=0;

protected:
    CommBase* port;

private:


};

#endif // SENSORS_H
