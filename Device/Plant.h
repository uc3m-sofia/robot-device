#ifndef PLANT_H
#define PLANT_H

#include "Actuator.h"
#include "Sensor.h"

class Plant
{
public:
//    Plant();
    Plant(Actuator *newActuator = NULL, Sensor *newSensor = NULL);

    double Update(double newInput);

    const double& x=_x; // Interface to variable and its derivatives (read only)
    const double& dx=_dx;
    const double& ddx=_ddx;


private:

    Actuator* input;
    Sensor* state;

    double _x=0,_dx=0,_ddx=0; // Main variable (x) and its derivatives

    long tmperr;
};

#endif // PLANT_H
