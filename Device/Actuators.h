#ifndef ACTUATORS_H
#define ACTUATORS_H


#include <vector>

#include "../Comms/Comms.h"

using namespace std;

class Actuators
{
public:
    Actuators(CommBase &nport);
    virtual double SetThrottle(vector<double>)=0;

protected:
    CommBase* port;

private:


};

#endif // ACTUATORS_H
