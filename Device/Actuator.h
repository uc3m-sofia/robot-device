#ifndef ACTUATOR_H
#define ACTUATOR_H

#include <string>

///
/// \brief The Actuator class
/// This encapsulate a single actuator having a throttle value.

class Actuator{
public:
    Actuator();
    virtual void set(std::string type,double* value,double n_pameters=1)=0;
    virtual long SetThrottle(double)=0;
};

#endif // ACTUATOR_H
