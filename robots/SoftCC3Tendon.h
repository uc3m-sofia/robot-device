#ifndef SOFTCC3TENDON_H
#define SOFTCC3TENDON_H


#include "Device/Robot.h"

///
/// \brief The SoftCC3Tendon class
/// Encapsulation of a soft link actuated by three tendons

using namespace std;


class SoftCC3Tendon : public Robot
{
public:

    ///
    /// \brief SoftCC3Tendon Class constructor
    /// \param nl0 Initial soft limb length (unactuated).
    /// \param nd Distance from center to tendons in a limb cross section.
    /// \param newTAngles The tendon separation angles. First angle defines the first tendon initial
    /// angle (usually zero) in a cross section of the soft link. The following angles define the angular
    /// distance from zero. For instance, a three tendon link equally spaced starting at 0 deg is
    /// defined as {0, 2*M_PI/3, 4*M_PI/3} (the default value).
    ///
    SoftCC3Tendon(double nl0, double nd, vector<double> newTAngles = {0, 2*M_PI/3, 4*M_PI/3});


    long IK(vector<double> rpl, vector<double> &tLengths);

    ///
    /// \brief SetRollPitchArcLen : Change the robot position to a new one defined by Roll and Pitch angles
    /// and Arc length. Arc length is the soft limb (curved) desired length.
    /// \param rpl : Vector with Roll and Pitch angles [rad] and Arc length [m].
    /// \return :  Zero on success, negative on errors, positive on warnings or values.
    ///
    long SetRollPitchArcLen(vector<double> rpl);

    vector<double> GetTendons();


private:

    double d;
    double l0;

    vector<double> alfas; //Tendon Angles[rad]
    vector<double> tendons; //Tendon lengths [m]
    double x,y,z; //Tip cartesian position[m]
    double i,o,l; //Tip inclination orientation position[rad, rad, m]
    vector<double> xyz; //Tip cartesian position[m]
    vector<double> iol; //Tip inclination orientation position[rad, rad, m]


    vector<double> xyz2iol(double x, double y, double z);
    vector<double> iol2xyz(vector<double> iol);
    vector<double> iol2xyz(double i, double o, double l);
};

#endif // SOFTCC3TENDON_H
