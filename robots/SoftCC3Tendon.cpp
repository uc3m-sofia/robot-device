#include "SoftCC3Tendon.h"


SoftCC3Tendon::SoftCC3Tendon(double nl0, double nd, vector<double> newTAngles)
{

    l0=nl0;
    d=nd;
    alfas = newTAngles;


    xyz.resize(3);
    iol.resize(3);
    tendons.resize(3);

}

long SoftCC3Tendon::IK(vector<double> rpl, vector<double> &tLengths)
{

    // double R=23E-3; //winch radius
    double r=0.05; //section radius (distance from center to tendons)
    //angle factor between servos and output angle
    // double fa=(r/R) * 4096.0/(2.0*M_PI); //[rad] * [pulses/rad]
    // double fl=(1/R) * 4096.0/(2.0*M_PI); //[m] * [pulses/rad]
    double l0=0.09; // bottom module 9 cm initial height



    double roll=rpl[0];
    double pitch=rpl[1];
    double len = rpl[2];

    //zero orientation points in the negative x axis direction
    double inc = atan(sqrt(tan(roll)*tan(roll)+tan(pitch)*tan(pitch)));
    double ori = fmod( atan2(pitch,roll), 2.0*M_PI);
    //    double inc2 = xyz_rp[3];
    //    double ori2 = xyz_rp[4];
    cout << "-> inc, ori, len: " << inc << ", " << ori << ", " << len << endl;

    tLengths[0]=r*inc*cos(ori+alfas[0])+(len-l0);
    tLengths[1]=r*inc*cos(ori+alfas[1])+(len-l0);
    tLengths[2]=r*inc*cos(ori+alfas[2])+(len-l0);

    if( (len-inc*r) < (l0/2) )
    {
        cout << "IK: Out of bounds -> inc, ori, len: " << inc << ", " << ori << ", " << len << endl;
        return -1;
    }




    return 0;

}



long SoftCC3Tendon::SetRollPitchArcLen(vector<double> rpl)
{

    double roll=rpl[0];
    double pitch=rpl[1];
    double len = rpl[2];


    l=len;

    if (roll==0 & pitch==0)
    {
        o=0;i=0;
        x=0;y=0;z=len;
    }
    else if(roll!=0 & pitch==0)
    {
        x=0;
        y=-(l/roll)*(1-cos(roll));

        i=abs(roll);
        o=atan2(y,x);

        z=l*sin(i)/i;

        // i=2*atan2(max(x/cos(o),y/sin(o)),z);

    }
    else if(roll==0 & pitch!=0)
    {
        x=(l/pitch)*(1-cos(pitch));
        y=0;

        o=atan2(y,x);
        i=abs(pitch);

        z=l*sin(i)/i;
    }
    else
    {

        x=(l/pitch)*(1-cos(pitch));
        y=-(l/roll)*(1-cos(roll));

        o=atan2(y,x);
        i=max(abs(roll),abs(pitch));

        z=l*sin(i)/i;

        // i=2*atan2(max(x/cos(o),y/sin(o)),z);

    }


    cout << "-> x, y, z: " << x << ", " << y << ", " << z << endl;

    cout << "-> inc, ori, len: " << i << ", " << o << ", " << l << endl;

    tendons[0]=-d*i*cos(o+alfas[0])+(l-l0);
    tendons[1]=-d*i*cos(o+alfas[1])+(l-l0);
    tendons[2]=-d*i*cos(o+alfas[2])+(l-l0);

    return 0;
}

vector<double> SoftCC3Tendon::GetTendons()
{
    return tendons;

}

vector<double> SoftCC3Tendon::xyz2iol(double x, double y, double z)
{
    vector<double> ret(3);


    double o=atan2(y,x);
    //discard singularities
    double pxy=max(x/cos(o),y/sin(o));

    //    //avoid zero division issues
    //    if(!isnormal(pxy))
    //    {
    //        if (cos(o)!=0) pxy=max(x/cos(o),y/sin(o));
    //        else
    //    }

    //    double i=2*atan(pxy/z); //tan always positive.
    double i=2*atan2(pxy,z); //tan always positive, but atan2 deals with NaN.
    double l=0;

    if (i==0) l=z;
    else l=pxy*i/(1-cos(i));

    ret[0]=i;
    ret[1]=o;
    ret[2]=l;


    return ret;
}

vector<double> SoftCC3Tendon::iol2xyz(vector<double> iol)
{

    vector<double> ret(3);

    double a=iol[0];
    double p=iol[1];
    double l=iol[2];

    if (a==0)
    {
        ret[2]=l;ret[0]=0;ret[1]=0;
    }
    else
    {
        ret[0]=l*cos(p)*(1-cos(a))/a;
        ret[1]=l*sin(p)*(1-cos(a))/a;
        ret[2]=l*sin(a)/a;
    }

    return ret;
}

vector<double> SoftCC3Tendon::iol2xyz(double i, double o, double l)
{

    vector<double> ret(3);

    if (i==0)
    {
        ret[2]=l;ret[0]=0;ret[1]=0;
    }
    else
    {
        ret[0]=l*cos(o)*(1-cos(i))/i;
        ret[1]=l*sin(o)*(1-cos(i))/i;
        ret[2]=l*sin(i)/i;
    }

    return ret;
}
