#ifndef COMMBASE_H
#define COMMBASE_H

#include <string>

using namespace std;
class CommBase
{
public:
    CommBase();
    virtual long send(string bytes)=0;
    virtual string receive()=0;
};

#endif // COMMBASE_H
