#include "SWave.h"

SWave::SWave(CommBase &nport) : Actuators(nport)
{
    //    port=&nport;
    throttles.resize(5,0); //initialize controlled robot outputs
    states.resize(6,0); //initialize tendon states

    iol_rp.resize(5);

    th2.resize(2);
    st1.resize(3);
    st2.resize(2);

    mvfail = 0x0;
//    mvfail[0]=true;
    if (mvfail!=0) cout << mvfail << endl;
//    cout << st2[0] << ", "<< st2[1] << ", "<< st2[2] << endl;
}


double SWave::SetThrottle(vector<double> nthrottles)
{

    //angle factor between servos and output angle
//    double fang=4096.0*(18.75E-3/23E-3)/(2.0*M_PI);
//    double flen=4096.0*(1/23E-3)/(2.0*M_PI);


//    double len0=(1.0/3.0)*(states[0]+states[1]+states[2])/flen;

//    double tanp=(cos(2*M_PI/3)-((states[2]-len0)/(states[1]-len0)))*(1/sin(2*M_PI/3));
//    double ori0=atan(tanp);
//    double inc0=(states[1]-len0)/(fang*cos(ori0));
//    cout << "inc, ori, len: " << inc0 << ", " << ori0 << ", " << len0 << endl;

    //Return error when out of limits
    if (nthrottles.size()!=5 |
        0)
    {
        cerr << "Wrong input" << endl;
        return -1;
    }



    if (IK(nthrottles, states)!=0) return -1;

    mvfail[1]=abs(states[0])>1024 | abs(states[1])>1024 | abs(states[2])>1024;
    mvfail[2]=abs(states[3])>2048 | abs(states[4])>2048 | abs(states[5])>2048;
    if ( mvfail !=0 )
    {
        cerr << "Wrong states: ";
        for (int i=0;i<states.size();i++)
        {
            cerr  << states[i] << ", " ;
        }
        cerr  << endl;
        return -1;
    }

    throttles = nthrottles;


//    double inc1=throttles[0];
//    double ori1=throttles[1];
//    double len1=throttles[2]*flen;


//    states[0]=inc1*fang*cos(ori1-2*M_PI/3)+len1;
//    states[1]=inc1*fang*cos(ori1+2*M_PI/3-2*M_PI/3)+len1;
//    states[2]=inc1*fang*cos(ori1+4*M_PI/3-2*M_PI/3)+len1;

//    double inc2=throttles[3];
//    double ori2=throttles[4];
//    double len2=throttles[2]*flen;

//    states[3]=inc2*fang*cos(ori2-2*M_PI/3)+len2;
//    states[4]=inc2*fang*cos(ori2+2*M_PI/3-2*M_PI/3)+len2;
//    states[5]=inc2*fang*cos(ori2+4*M_PI/3-2*M_PI/3)+len2;

    //send to SWave robot
    string message("E");
    int tendon=0;

    //    cout << ", " << fang << endl;

    for (int i=0; i<states.size(); i++)
    {
        tendon = (int)(states[i]);
        //        cout << nthrottles[i] << ", " << fang << endl;
        message = message + to_string(tendon) + " ";
    }

//    cout << message << endl;

    port->send(message);

    //    cout << "Arduino response: " << port->receive() << endl;
    //    cout << "Arduino response: " << port->receive() << endl;

    //    string recstr = port->receive();
    //    double ret=stod(recstr);
    //    cout << "Pos: " << ret;
    //    cout << "recstr: " << recstr << endl;

    return 0.0;

}

vector<double> SWave::DK(vector<double> nstate)
{
    vector<double> ret(3);

    return ret;
}

long SWave::IK(vector<double> xyz_rp, vector<double>& motors)
{

    double R=23E-3; //winch radius
    double r=18.75E-3; //section radius (distance from center to tendons)
    //angle factor between servos and output angle
    double fa=(r/R) * 4096.0/(2.0*M_PI); //[rad] * [pulses/rad]
    double fl=(1/R) * 4096.0/(2.0*M_PI); //[m] * [pulses/rad]
    double l0=0.09; // bottom module 9 cm initial height



    double roll=xyz_rp[3];
    double pitch=xyz_rp[4];
    //zero orientation points in the negative x axis direction
    double inc2 = atan(sqrt(tan(roll)*tan(roll)+tan(pitch)*tan(pitch)));
    double ori2 = fmod( (atan2(pitch,roll)+3.0*M_PI/2), 2.0*M_PI);
//    double inc2 = xyz_rp[3];
//    double ori2 = xyz_rp[4];
    double len2 = l0;

    if( (len2-inc2*r) < (l0/2) )
    {
        cout << "IK2: Out of bounds -> inc2, ori2, len2: " << inc2 << ", " << ori2 << ", " << len2 << endl;
        return -1;
    }

//        cout << "inc2, ori2, len2: " << inc2 << ", " << ori2 << ", " << len2 << endl;

    vector<double> xyz2=iol2xyz(inc2,ori2,len2);

//    cout << "xyz2: " << xyz2[0] << ", " << xyz2[1] << ", " << xyz2[2] << endl;


    vector<double> iol=xyz2iol(xyz_rp[0]-xyz2[0],xyz_rp[1]-xyz2[1],xyz_rp[2]-xyz2[2]);




//    vector<double> motors(6,0);

    double inc=iol[0];
    double ori=iol[1];
    double len=iol[2];

//    cout << "inc, ori, len: " << inc << ", " << ori << ", " << len << endl;

    if( (len-inc*r) < (l0/2) )
    {
        cout << "IK: Out of bounds -> inc, ori, len: " << inc << ", " << ori << ", " << len << endl;
        return -1;
    }




    double z=0.0;


    motors[0]=fa*inc*cos(ori-2*M_PI/3)+fl*(len-l0);
    motors[1]=fa*inc*cos(ori+2*M_PI/3-2*M_PI/3)+fl*(len-l0);
    motors[2]=fa*inc*cos(ori+4*M_PI/3-2*M_PI/3)+fl*(len-l0);

//    //use this code to block first module motion at zero.
//    motors[0]=0;
//    motors[1]=0;
//    motors[2]=0;
//    len=l0+0.01;

    motors[3]=fa*inc2*cos(ori2-2*M_PI/3)+fl*(len-l0);
    motors[4]=fa*inc2*cos(ori2+2*M_PI/3-2*M_PI/3)+fl*(len-l0);
    motors[5]=fa*inc2*cos(ori2+4*M_PI/3-2*M_PI/3)+fl*(len-l0);




//    motors[4]=( pitch/1.5 + z )*fa + fl*(len-l0);
//    motors[5]=( roll/1.732 - pitch/3.0 + z )*fa + fl*(len-l0);
//    motors[3]=( -roll/1.732 - pitch/3.0 + z )*fa + fl*(len-l0);

    return 0;
}

vector<double> SWave::xyz2iol(vector<double> xyz)
{

    return xyz2iol(xyz[0], xyz[1], xyz[2]);
}

vector<double> SWave::xyz2iol(double x, double y, double z)
{
    vector<double> ret(3);


    double o=atan2(y,x);
    //discard singularities
    double pxy=max(x/cos(o),y/sin(o));

//    //avoid zero division issues
//    if(!isnormal(pxy))
//    {
//        if (cos(o)!=0) pxy=max(x/cos(o),y/sin(o));
//        else
//    }

//    double i=2*atan(pxy/z); //tan always positive.
    double i=2*atan2(pxy,z); //tan always positive, but atan2 deals with NaN.
    double l=0;

    if (i==0) l=z;
    else l=pxy*i/(1-cos(i));

    ret[0]=i;
    ret[1]=o;
    ret[2]=l;


    return ret;
}

vector<double> SWave::iol2xyz(vector<double> iol)
{

    vector<double> ret(3);

    double a=iol[0];
    double p=iol[1];
    double l=iol[2];

    if (a==0)
    {
        ret[2]=l;ret[0]=0;ret[1]=0;
    }
    else
    {
        ret[0]=l*cos(p)*(1-cos(a))/a;
        ret[1]=l*sin(p)*(1-cos(a))/a;
        ret[2]=l*sin(a)/a;
    }

    return ret;
}

vector<double> SWave::iol2xyz(double i, double o, double l)
{

    vector<double> ret(3);

    if (i==0)
    {
        ret[2]=l;ret[0]=0;ret[1]=0;
    }
    else
    {
        ret[0]=l*cos(o)*(1-cos(i))/i;
        ret[1]=l*sin(o)*(1-cos(i))/i;
        ret[2]=l*sin(i)/i;
    }

    return ret;
}
/*double SWave::OldSetThrottle(vector<double> nthrottles)
{

    //angle factor between servos and output angle
    double fang=4096.0*(0.011/0.01)/(2.0*M_PI);


    //store the new throttles
    if (nthrottles.size()==5) throttles = nthrottles;
    else {cerr << "Wrong input" << endl; return -1;}

    double roll=throttles[3];
    double pitch=throttles[4];
    double z=throttles[2]/2;


    states[4]=( pitch/1.5 + z )*fang;
    states[3]=( roll/1.732 - pitch/3.0 + z )*fang;
    states[5]=( -roll/1.732 - pitch/3.0 + z )*fang;

    double mpitch=-throttles[0];
    double mroll=throttles[1];

    states[1]=( mpitch/1.5 + z )*fang ;
    states[0]=( mroll/1.732 - mpitch/3.0 + z )*fang;
    states[2]=( -mroll/1.732 - mpitch/3.0 + z )*fang;

    //send to SWave robot
    string message("E");
    int tendon=0;

    //    cout << ", " << fang << endl;

    for (int i=0; i<states.size(); i++)
    {
        tendon = (int)(states[i]);
        //        cout << nthrottles[i] << ", " << fang << endl;
        message = message + to_string(tendon) + " ";
    }

            cout << message << endl;

    port->send(message);

    //    cout << "Arduino response: " << port->receive() << endl;
    //    cout << "Arduino response: " << port->receive() << endl;

    //    string recstr = port->receive();
    //    double ret=stod(recstr);
    //    cout << "Pos: " << ret;
    //    cout << "recstr: " << recstr << endl;

    return 0.0;

}*/
