#ifndef ARDUINOSERVO_H
#define ARDUINOSERVO_H


#include <iostream>
#include <unistd.h>
#include <bitset>

#include <valarray>
#include <utility>

#include "../Device/Actuators.h"

class SWave : public Actuators
{
public:
    SWave(CommBase& nport);
    double SetThrottle(vector<double> nthrottles);

private:
    void set(std::string type,double* value,double n_pameters=1);
    vector<double> DK(vector<double> nstate);
//    vector<double> IK(vector<double> xyz_rp);
    long IK(vector<double> xyz_rp, vector<double> &motors);



    vector<double> xyz2iol(vector<double> xyz);
    vector<double> xyz2iol(double x, double y, double z);

    vector<double> iol2xyz(vector<double> apl);
    vector<double> iol2xyz(double i, double o, double l);


    vector<double> throttles;
    vector<double> iol_rp;

    vector<double> th1;
    vector<double> th2;
    vector<double> states;
    vector<double> st1;
    vector<double> st2;

    bitset<4> mvfail;



};

#endif // ARDUINOSERVO_H
