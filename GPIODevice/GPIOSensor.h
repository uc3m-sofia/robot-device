#ifndef GPIOSENSOR_H
#define GPIOSENSOR_H

#include "../Device/Sensor.h"

class GPIOSensor : public Sensor
{
public:
    GPIOSensor();
};

#endif // GPIOSENSOR_H
