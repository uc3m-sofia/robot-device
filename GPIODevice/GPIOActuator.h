#ifndef GPIOACTUATOR_H
#define GPIOACTUATOR_H

#include "../Device/Actuator.h"

class GPIOActuator : public Actuator
{
public:
    GPIOActuator();
};

#endif // GPIOACTUATOR_H
