#include "servo.h"

void Servo::setTargetPos(double targetPos){
    this->TargetPos = targetPos;
}

void Servo::updatePos(){

    ErrorPos = TargetPos - this->enc.get_pos();

    ControlPos = ErrorPos > PID_Pos;

    TargetVel = ControlPos;

    ErrorVel = TargetVel - this->enc.get_vel();

    ControlVel = ErrorVel > PID_Vel;

    motor.setThrottle(ControlVel);
}

double Servo::getPos(){
    return this->enc.get_pos();
}

void Servo::disableServo(){
    motor.setThrottle(0);
}
