#ifndef SERVO_H
#define SERVO_H

#include "QuadEncoder.h"
#include "H2Driver.h"
//#include "fcontrol.h" No tengo muy claro como añadir fcontrol aqui

class Servo
{
public:
    Servo(QuadEncoder encoder,H2Driver m , PIDBlock velocity, PIDBlock position):
        motor(m) ,enc(encoder), PID_Vel(velocity), PID_Pos(position)
    {
        PID_Vel.AntiWindup(-100,100);
        //motor = m;
    }

    ~Servo(){
        cout << "Servo destroyed" << endl;
    }

private:
    double TargetVel;
    double TargetPos;
    double ControlVel;
    double ControlPos;
    double ErrorVel;
    double ErrorPos;

    H2Driver motor;
    QuadEncoder enc;
    PIDBlock PID_Vel;
    PIDBlock PID_Pos;
    SamplingTime Ts;

public:
    void setTargetPos(double targetPos);
    void updatePos();
    double getPos();
    void disableServo();


};

#endif
