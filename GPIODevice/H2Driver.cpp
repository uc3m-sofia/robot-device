#include "H2Driver.h"

H2Driver::H2Driver(int new_pinAIN1, int new_pinAIN2, int new_pinSTBY, int new_pinPWM){

    pins.push_back(new_pinAIN1);
    pins.push_back(new_pinAIN2);
    pins.push_back(new_pinSTBY);
    pins.push_back(new_pinPWM);

    for(int i= 0; i< pins.size()-1; i++){
        gpios.push_back(libsoc_gpio_request(pins[i], LS_GPIO_SHARED));

    }

    for(int i = 0; i < gpios.size(); i++){
        libsoc_gpio_set_direction(gpios[i], OUTPUT);
        libsoc_gpio_set_level(gpios[i],LOW);
    }

    p = libsoc_pwm_request(0,pins[3], LS_PWM_GREEDY);

    int i = 0;
    while (p == NULL){
        p = libsoc_pwm_request(0,pins[3], LS_PWM_GREEDY);
        i++;
        cout << "PWM requested " << i << " times" << endl;

    }


    cout << "Enabling PWM and Stanby pin" << endl;
    freqPWM = 180; //in Hz (jetson max=187 Hz?) Test confirms that Hz>180 reduce velocity
    periodPWM = uint(1000*1000/freqPWM); //in nanoseconds
    enablePWM(periodPWM);
    enable();
    cout << "freqPWM : " << freqPWM << ", periodPWM : " << periodPWM <<" nanoseconds." << endl;

}

H2Driver::~H2Driver(){

    for(int i=0; i<pins.size()-1;i++){
        libsoc_gpio_set_level(gpios[i],LOW);
        libsoc_gpio_free(gpios[i]);
    }
    libsoc_pwm_free(p);
}

void H2Driver::SetPWM(double period, double duty){ //Receive duty as percent and convert later

    libsoc_pwm_set_period(p,period);
    libsoc_pwm_set_enabled(p,ENABLED);

    if (duty > 100){
        printf("Duty cycle cannot be higher than period\n");
        exit(EXIT_FAILURE);
    }
    else{
        //Duty must be an integer
        int newduty=(period*duty/100);
        libsoc_pwm_set_duty_cycle(p,newduty);
    }
}

void H2Driver::set(string type, double* value,double n_parameters)
{
    if(type=="PWM"){
        SetPWM(value[0],value[1]);

    }else if(type=="throttle"){
        SetThrottle(value[0]);
    }
}

void H2Driver::disablePWM(){
    statePWM = libsoc_pwm_get_enabled(p);

    if(statePWM == ENABLED){
        libsoc_pwm_set_enabled(p,DISABLED);
    }
}

void H2Driver::enablePWM(long new_period)
{
    libsoc_pwm_set_period(p,new_period);

//    SetPWM(new_period,0);

    tmpret = libsoc_pwm_set_enabled(p,ENABLED);
        cout << "tmpret"<< tmpret << endl;

    statePWM = libsoc_pwm_get_enabled(p);

    if(statePWM == DISABLED)
    {
        cerr << "PWM disabled." << endl;
        exit(EXIT_FAILURE);
    }

}

void H2Driver::enable()
{

    //set enable pin high
    libsoc_gpio_set_level(gpios[2],HIGH);
    //set standard bridge direction
    libsoc_gpio_set_level(gpios[0],HIGH);
    libsoc_gpio_set_level(gpios[1],LOW);
}

void H2Driver::disable(){
    //set enable pin low
    libsoc_gpio_set_level(gpios[2],LOW);
}

long H2Driver::SetThrottle(double nThrottle)
{

    if (abs(nThrottle)<100)
    {
        duty=(uint)(periodPWM*abs(nThrottle)/100);
        //    cout << "periodPWM: " << periodPWM << endl;
        //    cout << "duty: " << duty << endl;

    }
    else
    {
        duty=(uint)(periodPWM);
        cerr << "Saturation. maxThrottle: 100, current: " << nThrottle << endl;

    }

    if (nThrottle < 0)
    {
        libsoc_gpio_set_level(gpios[0],LOW);
        libsoc_gpio_set_level(gpios[1],HIGH);
//        changeDutyCycle(abs(nThrottle));


    }
    else
    {
        libsoc_gpio_set_level(gpios[0],HIGH);
        libsoc_gpio_set_level(gpios[1],LOW);
//        changeDutyCycle(abs(nThrottle));

    }

    libsoc_pwm_set_duty_cycle(p,duty);
//    cout << "GetThrottle: " << libsoc_pwm_get_duty_cycle(p) << endl;


    return 0;
}
