#ifndef H2DRIVER_H
#define H2DRIVER_H

#include <iostream>
#include <libsoc_gpio.h>
#include <libsoc_pwm.h>
#include <vector>
#include "GPIOActuator.h"
using namespace std;

class H2Driver: public Actuator{
public:
    H2Driver(int new_pinAIN1, int new_pinAIN2, int new_pinSTBY, int new_pinPWM);
    ~H2Driver();
    void SetPWM(double period, double duty);
    void set(string type,double* value,double n_pameters);
//    int setThrottle(double duty);
    void disablePWM();
    void enablePWM(long new_period);
    void enable();
    void disable();
    long SetThrottle(double nThrottle);

private:
    vector<int> pins;
    vector<gpio*> gpios;
    pwm* p;
    pwm_enabled statePWM;
    long freqPWM;
    uint periodPWM;
    uint duty;

    long tmpret;
};


#endif
