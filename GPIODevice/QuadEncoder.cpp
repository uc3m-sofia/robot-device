#include "QuadEncoder.h"

//void loop(bool & ready)
//{


//    while (ready==1)
//    {
//        sleep(1);

//    }
//}

long _pulses =0;
long * pulses = &(_pulses);
QuadEncoder* cbEncoder;


// This function should work for any board hardware, so it will be called
// from new_board GPIOBoard in the constructor
//int QuadEncoder::callbackA(int argc, char* argv)
int EdgeA(int inputlvl, void* thisptr)
{
    cbEncoder=(QuadEncoder*)thisptr;
    cbEncoder->EdgeOnA(inputlvl);


//    cbEncoder->pulses=cbEncoder->pulses+1;
//    cout<< "interrupt at : " << inputlvl << endl;
    //    *pulses=*pulses+1;
    //    this->pulses+=;
    return 0;
}


int EdgeB(int inputlvl, void* thisptr)
{
    cbEncoder=(QuadEncoder*)thisptr;
    cbEncoder->EdgeOnB(inputlvl);
//        cout<< "interrupt at : " << inputlvl << endl;

    return 0;
}

//int FallingA(int argc, void* argv)
//{
//cbEncoder=(QuadEncoder*)argv;
//        cbEncoder->pulses=cbEncoder->pulses-1;
//    cout<< "interrupt at : " << cbEncoder->pulses << endl;
////    *pulses=*pulses+1;
////    this->pulses+=;
//    return 0;
//}


QuadEncoder::QuadEncoder(int new_pinA, int new_pinB, GPIOBoard &new_board, int pulse_per_rev, double sample_time)
{

    board = &new_board;

    pins[0]=new_pinA;
    pins[1]=new_pinB;

    callbacks[0]=EdgeA;
    callbacks[1]=EdgeB;

//    callbacks[1]=FallingA;
//    auto fn2=mem_fn(&QuadEncoder::callbackA);


    board->InterruptOnEdge(pins[0], this, callbacks[0]);
    board->InterruptOnEdge(pins[1], this, callbacks[1]);

    pulses=0;
    A=0;
    B=0;

    prev_pulses = 0;

    ppr = pulse_per_rev;
    ts = sample_time;



    SetAverageFilter(10);

//    tloop = thread(loop, std::ref(ready));


    pins[0]=new_pinA;
    pins[1]=new_pinB;

    ready = 1;
/*
    chip = gpiod_chip_open("/dev/gpiochip0");
    if(!chip)
    {
      perror("gpiod_chip_open");
    }
    else
    {
        cout << gpiod_chip_name(chip);
        cout << ", open correct from QuadEncoder." << endl;
    }

    lines=&line_bulk;
    gpiod_line_bulk_init(lines);
//    lines = gpiod_chip_get_lines(chip, pins, nlines ); //new way

    for (int i=0; i<2; i++)
    {
        line=gpiod_chip_get_line(chip, pins[i]);

        gpiod_line_bulk_add(lines, line );
        cout << gpiod_line_bulk_num_lines(lines) << std::endl;

    }



    if (gpiod_line_request_bulk_both_edges_events(lines, "QuadEncoder") < 0)
    {
        perror("Request event notification failed\n");
        std::cout << "Request event notification failed\n" <<   std::endl;

    }

    tloop = thread(loop, std::ref(ready),std::ref(lines));

>>>>>>> f198d50f3fb0b396e92119937a8335df58ddfbdb
*/
//    thread tloop(loop);
//    cout<<  tloop.joinable() << endl;




}

QuadEncoder::~QuadEncoder()

{
//    ready=0;
//    tloop.join();

//    gpiod_line_release_bulk(lines);
    //    gpiod_chip_close(chip);
}

long QuadEncoder::SetAverageFilter(int nSize)
{
    oldPulses = valarray<long>(long(0),nSize);

    return 0;
}

void QuadEncoder::EdgeOnA(int inputValue)
{
    A=inputValue;
    if(A==B)
    {
        pulses--;
    }
    else
    {
        pulses++;
    }

}

void QuadEncoder::EdgeOnB(int inputValue)
{
    B=inputValue;
    if(A==B)
    {
        pulses++;
    }
    else
    {
        pulses--;
    }

}

double QuadEncoder::get(string type_value)
{
    if(type_value=="pos"){
        return get_pos();
    }else if(type_value=="vel"){
        return get_vel();
    }else
        return 0;
}

double QuadEncoder::get_pos(){
    return pulses*2*M_PI/ppr;
}

double  QuadEncoder::get_vel()
{
    speed = (double(pulses - prev_pulses)*2*M_PI/ppr)/ts;

    prev_pulses = pulses;

    return speed;
}

double QuadEncoder::GetFVel()
{

    prevAvg = pulseAvg;

    oldPulses = oldPulses.shift(-1);
    oldPulses[0] = pulses;
    pulseAvg = double(oldPulses.sum())/oldPulses.size();

//    cout << "pulseAvg: " << pulseAvg << ",  pulses: " << pulses << endl;
//    cout << "oldPulses.sum(): " << oldPulses.sum() << ",  /oldPulses.size(): " << oldPulses.size() << endl;


    speed = ((pulseAvg - prevAvg)/ts)*2*M_PI/ppr;


    return speed;

}
