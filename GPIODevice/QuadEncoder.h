#ifndef QUADENCODER_H
#define QUADENCODER_H

#include <string>
#include <valarray>
//#include <thread>

//#include <unistd.h>
#include <functional>   // std::function, std::negate

#include <stdio.h> //perror
#include <iostream>

#include "GPIOBoard.h"
#include "GPIOSensor.h"

#define signalN 2

using namespace std;

class QuadEncoder: public Sensor
{
public:
    QuadEncoder(int new_pinA, int new_pinB, GPIOBoard & new_board, int pulse_per_rev=512, double sample_time=0.01);
    ~QuadEncoder();

    long SetAverageFilter(int nSize);

    void EdgeOnA(int inputValue);
    void EdgeOnB(int inputValue);
    double get(string type_value);
    double get_pos();
    double get_vel();
    double GetFVel();
    long pulses; //number of total pulses since init (can be negative)
    long prev_pulses;
//    QuadEncoder(int new_pinA, int new_pinB, string ioDevice = "/dev/gpiochip0");
//    ~QuadEncoder();


    //    int callbackA(int argc, char *argv);

private:

    bool ready;
//    thread tloop;

    double speed;
    valarray<long> oldPulses;
    double pulseAvg,prevAvg;

    int pins[2];
    GPIOBoard* board;
//    unsigned int pins[2];

//    gpiod_chip* chip;
//    gpiod_line_bulk line_bulk;
//    gpiod_line* line;
//    gpiod_line_bulk* lines;

    int values[signalN];
    unsigned int nlines=2;
//    gpiod_line* lineA;
//    gpiod_line* lineB; //gpiod lib input objects

    function<int(int,void*)> callbacks[4];

    int A,B; //encoder cuad signals

    int pinA,pinB;

    int ppr;

    double ts;

};

#endif // QUADENCODER_H
